<?php
/*
  ./app/routeur.php
 */

 /*
   DETAIL D'UN LOGIN
   PATTERN: /index.php?login
   CTRL: usersControleur
   ACTION: showForm
  */

 if (isset($_GET['users'])):
   include_once '../app/routeurs/usersRouteur.php';
   //\App\Controleurs\UsersControleur\loginFormAction();



 elseif (isset($_GET['categories'])):
   include '../app/routeurs/categoriesRouteur.php';

 elseif (isset($_GET['posts'])):
   //var_dump('bonjour');
   include '../app/routeurs/postsRouteur.php';






  /*
    ROUTE PAR DEFAUT
    PATTERN: /
    CTRL: postsControleur
    ACTION: index
   */
 else:
    include_once '../app/controleurs/postsControleur.php';
    \App\Controleurs\PostsControleur\indexAction($connexion, [
      'orderBy' => 'datePublication',
      'orderSens' => 'DESC'

    ]);
  endif;
