<?php
/*
    ./app/modeles/postsModele.php
 */
namespace App\Modeles\PostsModele;

function findAll(\PDO $connexion){
  $sql = "SELECT *, posts.id as postID /* création d alias sinon quand on cliquera sur le lien d un post le bon ne s affichera pas il prendrait l id de l auteur*/
          FROM posts
          JOIN auteurs ON posts.auteur = auteurs.id  /* Join pour aller chercher les auteurs et ne plus avoir l id a modifier en mettant pseudo dans la vue.*/
          ORDER BY datePublication DESC
          LIMIT 5;";

$rs = $connexion->query($sql);
return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

function findOneById(\PDO $connexion, int $id){
    $sql = "SELECT *
            FROM posts
            JOIN auteurs ON auteur = auteurs.id
            WHERE posts.id = :id;";  /* id devient posts.id car il est devenu un champ ambigu en etant juste id*/
    $rs = $connexion->prepare($sql);
    $rs->bindValue(':id', $id, \PDO::PARAM_STR); /* ICI on pourait creer une fonction rs pour economiser 4 lignes */
    $rs->execute();
    return $rs->fetch(\PDO::FETCH_ASSOC);
}


function findAllByCategorie(\PDO $connexion, $id){
   $sql =  "SELECT *, posts.id as postID, categories.titre AS categorieTitre, auteurs.id as auteurID
            FROM posts
            JOIN posts_has_categories ON posts.id = post
            JOIN categories ON categories.id = categorie
            JOIN auteurs ON auteur = auteurs.id
            WHERE categories.id = :id;";

            $rs = $connexion->prepare($sql);
            $rs->bindValue(':id', $id, \PDO::PARAM_STR); /* ICI on pourait creer une fonction rs pour economiser 4 lignes */
            $rs->execute();
            return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

function findAllBySearch (\PDO $connexion, string $search){
  //var_dump($search);
  $words = explode(' ', trim($search));// Décomposer le $search en plusieurs mots, donc $words qui est = a explode qui est une fonction php qui va agir
  //comme un ciseau. Donc ca veux dire que $words va etre un tableau indexé qui va reprendre toutes les parties de $search, que je coupe
  // avec un espace, l'espace il va couper et il va en faire un tiroir de mon tableau indexé. Donc j'aurais tous les mots séparés
  // dans un tiroir. Remarque on peux mettre un trim devant pour enlever les espaces avant et apres pour etre sur qu'il n'y ai pas d'espaces
  // avant, parce que sinon on risque d'avoir des mots vides etc.
  // Donc ca me fait un tableau indexé avec les différents mots de la recherche.
  $sql =  "SELECT DISTINCT
                      posts.id as postID,
                      posts.titre AS titrePost,
                      posts.slug AS slugPost,
                      texte, datePublication, media, pseudo
           FROM posts
           JOIN auteurs ON auteur = auteurs.id
           JOIN posts_has_categories ON post = posts.id
           JOIN categories ON categorie = categories.id
           WHERE 1 = 0 "; // conditions absurde pour pouvoir boucler tous mes OR.
           for ($i=0; $i<count($words); $i++): //je crée ma boucle pour mon tableau pour boucler tous mes OR
              $sql .= " OR posts.titre      LIKE :word$i
                       OR texte            LIKE :word$i
                       OR categories.titre LIKE :word$i
                       OR pseudo           LIKE :word$i";
           endfor;
           $sql .= ";"; // Ici je cloture ma chaine sql qui commence la au dessus.
//var_dump($sql);
           $rs = $connexion->prepare($sql);
           for ($i=0; $i<count($words); $i++):
             $rs->bindValue(":word$i", '%'.$words[$i].'%', \PDO::PARAM_STR); /* ICI on pourait creer une fonction rs pour economiser 4 lignes */
           endfor;
           $rs->execute();
           return $rs->fetchAll(\PDO::FETCH_ASSOC);
}
