<?php
    // ./app/routeurs/postsRouteur ;


use \App\Controleurs\PostsControleur;
include_once '../app/controleurs/postsControleur.php';

switch ($_GET['posts']):
  case 'show':
  /*
    DETAIL D'UN POST
    PATTERN: /index.php?post=show&d=x
    CTRL: postsControleur
    ACTION: show
   */

    PostsControleur\showAction($connexion, $_GET['id']);
    break;
  case 'search' :
  /*
    RECHERCHE D'UN POST
    PATTERN: /index.php?post=search
    CTRL: postsControleur
    ACTION: search
   */
    PostsControleur\searchAction($connexion, $_POST['search']);
    break;
endswitch;
