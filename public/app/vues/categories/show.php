<?php
/*
    ./app/vues/categories/show.php
    Variables disponibles:
    - $categories: ARRAY(id, titre, slug,)
    - Variables $posts ARRAY(ARRAY(id, titre, slug, media, texte, datePublication, auteur))
 */
 ?>
 <h1 class="page-header">
     Posts de la categorie
     <small><?php echo $posts[0]['categorieTitre']; ?></small> <!-- [0] me permettra de resoudre le probleme du au array dans un array -->
 </h1>


<?php foreach ($posts as $post): ?>

  <!-- Article -->
    <article>
       <h2>
           <a href="posts/<?php echo $post['postID']; ?>/<?php echo $post['slug']; ?>">
             <?php echo $post['titre']; ?></a> <!--Lien du titre-->
       </h2>
       <p class="lead">
         by <a href="#"><?php echo $post['pseudo']; ?></a>
       </p>
       <p> Posted on
         <?php echo \Noyau\Functions\formater_date ($post['datePublication']); ?>     </p>
       <hr>
       <img class="img-responsive z-depth-2" src="<?php echo $post['media']; ?>" alt="<?php echo $post['titre']; ?>">
       <hr>
          <div><?php echo \Noyau\Functions\tronquer($post['texte']); ?></div>
       <a href="posts/<?php echo $post['postID']; ?>/<?php echo $post['slug']; ?>"> <!--Lien du bouton-->
         <button type="button" class="btn btn-info waves-effect waves-light">Read more</button>
       </a>
       <hr>
    </article>
  <!-- Fin d'article -->
<?php endforeach; ?>
