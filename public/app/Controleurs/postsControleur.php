<?php
/*
    ./app/controleurs/postsControleur.php
 */
namespace App\Controleurs\PostsControleur;
use \App\Modeles\PostsModele AS Post;

function indexAction(\PDO $connexion) {
  include_once '../app/modeles/postsModele.php';
  $posts = Post\findAll($connexion);


  GLOBAL $content1, $title;
  $title = POSTS_INDEX_TITLE;
  ob_start();
    include '../app/vues/posts/index.php';
  $content1 = ob_get_clean();
}

// SHOW ACTION
function showAction(\PDO $connexion, int $id) {

  // Je demande le post au modèle.
  include_once '../app/modeles/postsModele.php';
  $post = Post\findOneById($connexion, $id);

  // Je charge la vue Show dans $content1
  GLOBAL $content1, $title;
  $title = $post['titre'];
  ob_start();
    include '../app/vues/posts/show.php';
  $content1 = ob_get_clean();
}

// SEARCH ACTION --> Recherche d'un post
function searchAction (\PDO $connexion, string $search){
  //Je demande la liste des posts au modèle
    include_once '../app/modeles/postsModele.php';
    $posts = Post\findAllBySearch($connexion, $search);
    //var_dump($posts);

  //Je charge la vue search dans content1
  //var_dump($posts);
  GLOBAL $content1, $title;
  $title = $search;
  ob_start();
    include '../app/vues/posts/search.php';
  $content1 = ob_get_clean();

}
