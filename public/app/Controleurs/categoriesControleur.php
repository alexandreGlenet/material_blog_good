<?php
/*
    ./app/controleurs/categoriesControleur.php
 */
namespace App\Controleurs\CategoriesControleur;
use \App\Modeles\CategoriesModele AS Categorie;

function indexAction(\PDO $connexion) {
  // Je demande la liste des categories au modele
  include_once '../app/modeles/categoriesModele.php';
  $categories = Categorie\findAll($connexion);

// Je charge directement la vue car ca va se mettre dans le templates sans passer par un content1

    include '../app/vues/categories/index.php';

}

// SHOW ACTION

function showAction(\PDO $connexion, int $id) {

  // Je demande la categorie au modèle.
  //include_once '../app/modeles/categoriesModele.php';
  //$categorie = Categorie\findOneById($connexion, $id);

  include_once '../app/modeles/postsModele.php';
  $posts = \App\Modeles\PostsModele\findAllByCategorie($connexion, $id);
  //print_r($posts);

  // Je charge la vue Show dans $content1
  GLOBAL $content1, $title;
  //$title = $categorie['titre'];
  ob_start();
    include '../app/vues/categories/show.php';
  $content1 = ob_get_clean();
}
