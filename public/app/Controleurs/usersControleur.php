<?php
/*
    ./app/controleurs/usersControleur.php
 */
namespace App\Controleurs\Users;
use \App\Modeles\User;

function loginFormAction(){

  GLOBAL $content1, $title;
  $title = TITRE_USERS_LOGINFORM;
  ob_start();
    include_once '../app/vues/users/loginForm.php';
  $content1 = ob_get_clean();

}

function loginAction(\PDO $connexion, array $data = null){
  // Je demande le User qui correspond au login/pwd
  include '../app/modeles/usersModele.php';
  $user = User\findOneByLoginPwd($connexion, $data);
  // Je redirige vers le backOffice si c'est ok et vers le formulaire de connexion sinon.
  if ($user):
    $_SESSION['user'] = $user; // On crée une variable de session, ceci sera pour la protection du site pour ne pas changer dans l'url le nom de dossier public par admin.
                                // en meme temps ca cree un tableau associatif. ici c'est un peu l'idée de mettre un cachet comme quand on rentre dans une soirée.
                                // attention des que l'on utilise une variable de session, ils faut aller dans sont fichier config ou init et indiquer voir int.php.
    header ('location:' . ROOT_ADMIN );
  else:
    header ('location:' . ROOT . 'users/login/form');
  endif;
}
