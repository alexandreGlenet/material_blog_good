<?php

// PARAMETRES DE CONNEXION A LA DB
  define('DB_HOST', 'localhost:3306');
  define('DB_NAME', 'material_blog');
  define('DB_USER', 'root');
  define('DB_PWD' , 'root');

// Chemins

  define('PUBLIC_FOLDER', 'public');
  define('ADMIN_FOLDER', 'admin'); // path = chemin en anglais.

// INITIALISATION DES ZONES DYNAMIQUES
   $content1 = '';
   $title    = '';

// AUTRES CONSTANTES

  //define('POSTS_INDEX_TITLE', "Lastest posts");
  //define('TITRE_USERS_LOGINFORM', "Connexion au backOffice");
  define('TITRE_USERS_DASHBOARD', "Dashboard");
  define('TITRE_CATEGORIES_INDEX', "Liste des catégories");
  define('TITRE_CATEGORIES_ADDFORM', "Ajout d'une catégories");
