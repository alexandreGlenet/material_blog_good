<?php
    // ./app/routeurs/categoriesRouteur ;


use \App\Controleurs\CategoriesControleur;
include_once '../app/controleurs/categoriesControleur.php';

switch ($_GET['categories']):
  case 'index':
  /*
    LISTE DES CATEGORIES
    PATTERN: /index.php?categories=index
    CTRL: categoriesControleur
    ACTION: index
   */
  CategoriesControleur\indexAction($connexion);
  break;
  case 'addForm':
  /*
    AJOUT CATEGORIES: FORMULAIRE
    PATTERN: /index.php?categories=addForm
    CTRL: categoriesControleur
    ACTION: addForm
   */
  CategoriesControleur\addFormAction();
  break;
  case 'add':
  /*
    AJOUT CATEGORIES: INSERT
    PATTERN: /index.php?categories=add
    CTRL: categoriesControleur
    ACTION: add
   */
  CategoriesControleur\addAction($connexion, [
    'titre' => $_POST['titre']
  ]);
  break;

endswitch;
