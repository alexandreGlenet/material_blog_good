<?php
/*
    ./app/controleurs/categoriesControleur.php
 */
namespace App\Controleurs\CategoriesControleur;
use \App\Modeles\CategoriesModele AS Categorie;

function indexAction(\PDO $connexion) {
// Je demande la liste des categories au modele
    include_once '../app/modeles/categoriesModele.php';
    $categories = Categorie\findAll($connexion);

// Je charge la vue index dans $content1
    GLOBAL $content1, $title;
    $title = TITRE_CATEGORIES_INDEX;
    ob_start();
      include '../app/vues/categories/index.php';
    $content1 = ob_get_clean();

}

function addFormAction(){
  // Je charge la vue index dans $content1
      GLOBAL $content1, $title;
      $title = TITRE_CATEGORIES_ADDFORM;
      ob_start();
        include '../app/vues/categories/addForm.php';
      $content1 = ob_get_clean();
}

function addAction(\PDO $connexion, array $data = null){
  // Je demande au modèle d'ajouter la catégorie
  include_once '../app/modeles/categoriesModele.php';
  $id = Categorie\insert($connexion, $data); // Insert = $id
  // Je redirige vers la liste des catégories Redirection = header location
  header('location: ' . ROOT . 'categories');
}
