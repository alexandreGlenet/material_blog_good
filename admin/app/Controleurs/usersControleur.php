<?php
/*
    ./app/controleurs/usersControleur.php
 */
namespace App\Controleurs\Users;
use \App\Modeles\User;

function dashboardAction(){
  // Je charge la vue dashboard dans $content1
  GLOBAL $content1, $title;
  $title = TITRE_USERS_DASHBOARD;
  ob_start();
    include_once '../app/vues/users/dashboard.php';
  $content1 = ob_get_clean();

}

function logoutAction(){
  // Je tue la variable de sessions 'user'
    unset($_SESSION['user']); // unset pour tuer les variable de session.
  // Je redirige vers le site public
    header('location: ' . ROOT_PUBLIC);
}
