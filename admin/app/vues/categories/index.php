<?php
/*
    ./app/vues/categories/index.php
    Variables disponibles:
    - $categories: ARRAY(ARRAY(id, titre, slug,))
 */
 ?>
 <h1><?php echo TITRE_CATEGORIES_INDEX; ?></h1>

 <div><a href="<?php echo ROOT; ?>categories/add/form">Ajouter un enregistrement</a></div>

 <table class="table table-bordered">
   <thead>
     <tr>
       <th>Id</th>
       <th>Titre</th>
       <th>Slug</th>
       <th></th>
     </tr>
   </thead>
   <tbody>
     <?php foreach ($categories as $categorie): ?>
       <tr>
         <td><?php echo $categorie ['id']; ?></td>
         <td><?php echo $categorie ['titre']; ?></td>
         <td><?php echo $categorie ['slug']; ?></td>
         <td>
           <a href="#">Edit</a> |
           <a href="#">Delete</a>
         </td>
       </tr>
     <?php endforeach; ?>


     </tbody>
 </table>
