<?php
/*
    ./app/vues/posts/search.php
    Variables disponibles:
    - $search STRING
    - $posts: ARRAY(ARRAY(id, titre, slug, datePublication, texte, media, auteur))
 */
 ?>
 <h1 class="page-header">
     Résultat de la recherche :
     <small><?php echo $search ?></small>
 </h1>
<?php //var_dump('hello') ?>
<?php foreach ($posts as $post): ?>

  <!-- Article -->
    <article>
       <h2>
           <a href="posts/<?php echo $post['postID']; ?>/<?php echo $post['slugPost']; ?>">
             <?php echo $post['titrePost']; ?></a> <!--Lien du titre-->
       </h2>
       <p class="lead">
         by <a href="#"><?php echo $post['pseudo']; ?></a>
       </p>
       <p> Posted on
         <?php echo \Noyau\Functions\formater_date ($post['datePublication']); ?>     </p>
       <hr>
       <img class="img-responsive z-depth-2" src="<?php echo $post['media']; ?>" alt="<?php echo $post['titrePost']; ?>">
       <hr>
          <div><?php echo \Noyau\Functions\tronquer($post['texte']); ?></div>
       <a href="posts/<?php echo $post['postID']; ?>/<?php echo $post['slugPost']; ?>"> <!--Lien du bouton-->
         <button type="button" class="btn btn-info waves-effect waves-light">Read more</button>
       </a>
       <hr>
    </article>
  <!-- Fin d'article -->
<?php endforeach; ?>
