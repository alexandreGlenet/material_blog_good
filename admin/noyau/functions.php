<?php

/* Fonction pour tronquer une chaine de caractère limite de 200 */
/* ./noyau/function.php*/

namespace Noyau\Functions;

/* TRAITEMENT DE CHAINES DE CARACTERES */
/**  // /** pour faire une description automatique grave a docblockr



/**
 * [tronquer description]
 * @param  string  $chaine         [description]
 * @param  integer $nbreCaracteres [description]
 * @return string                  [description]
 */

function tronquer(string $chaine, int $nbreCaracteres = 200) : string {
  if (strlen($chaine) > $nbreCaracteres) :
    $positionEspace = strpos($chaine, ' ', $nbreCaracteres);
    return substr($chaine, 0, $positionEspace);
  else:
    return $chaine;
  endif;
}

/* TRAITEMENT DES DATES */

/**
 * [formater_date description]
 * @param  string $date   [description]
 * @param  string $format [description]
 * @return string         [description]
 */

function formater_date(string $date, string $format = 'D d M Y') : string {
  return date_format(date_create($date), $format);
}

/**
 * [slugify description]
 * @param  string $str [description]
 * @return [type]      [description]
 */
function slugify(string $str) {
  return trim(preg_replace('/[^a-z0-9]+/', '-', strtolower($str)),'-');
}

?>
